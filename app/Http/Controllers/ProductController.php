<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Product;
use App\Models\Category;
use App\Models\Tag;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::with(['category'])->paginate(1);
        if(!empty($products)){
            foreach($products as $k => $v){
                $f = explode(",", $v->tag_id);
                $import_tag = Tag::whereIn('id', $f)->pluck('name');
                $products[$k]['tag_name'] = $import_tag;
            }
        }

        return Inertia::render('Product/Index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categorys = Category::all();
        $tags =Tag::all();
        return Inertia::render('Product/Create', ['categorys' => $categorys, 'tags' => $tags]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();

        Validator::make($input, [
            'name' => ['required'],
            'qty' => ['required'],
            'category_id' => ['required'],
            'amount' => ['required'],
            'tag_id' => ['required']
        ])->validate();

        $input['tag_id'] = !empty($input['tag_id']) ? implode(",", $input['tag_id']) : 0;

        Product::create($input);
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return Inertia::render('Product/Edit', [
            'product' => Product::find($id),
            'categorys' => Category::all(),
            'tags' => Tag::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $input = $request->all();
        Validator::make($input, [
            'name' => ['required'],
            'qty' => ['required'],
            'category_id' => ['required'],
            'amount' => ['required'],
            'tag_id' => ['required']
        ])->validate();

        $input['tag_id'] = !empty($input['tag_id']) ? implode(",", $input['tag_id']) : 0;

        Product::find($id)->update($input);
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Product::find($id)->delete();
        return redirect()->route('products.index');
    }
}
