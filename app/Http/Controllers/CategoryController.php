<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $categorys = Category::with(['parent'])->paginate(3);
        return Inertia::render('Category/Index', ['categorys' => $categorys]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categorys = Category::all();
        return Inertia::render('Category/Create', ['categorys' => $categorys]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Validator::make($input, [
            'name' => ['required'],
        ])->validate();
        $input['parent_category'] = !empty($input['parent_category']) ? $input['parent_category'] : 0;

        Category::create($input);
        return redirect()->route('categorys.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        return Inertia::render('Category/Edit', [
            'category' => Category::find($id),
            'categorys' => Category::where('id','!=',$id)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $input = $request->all();
        Validator::make($input, [
            'name' => ['required'],
        ])->validate();
        $input['parent_category'] = !empty($input['parent_category']) ? $input['parent_category'] : 0;

        Category::find($id)->update($input);
        return redirect()->route('categorys.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::find($id);
        $subcategories = $category->allSubcategories;

        if(!empty($subcategories)){
            $id = (int)$id;
            $ids = array($id);
            foreach($subcategories as $k => $v){
                array_push($ids, $v->id);
            }
        }
        if(!empty($ids)){
            Category::whereIn('id', $ids)->delete();
            Product::whereIn('category_id', $ids)->delete();
        }
        return redirect()->route('categorys.index');
    }
}
