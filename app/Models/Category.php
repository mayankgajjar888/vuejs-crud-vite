<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'category';

    protected $fillable = [
        'name','parent_category'
    ];

    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_category', 'id');
    }

    /*public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }*/

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_category');
    }

    public function allSubcategories()
    {
        return $this->children()->with('allSubcategories');
    }

    public function getAllSubcategoriesRecursive()
    {
        $subcategories = $this->children;
        foreach ($subcategories as $subcategory) {
            $subcategories = $subcategories->merge($subcategory->getAllSubcategoriesRecursive());
        }
        return $subcategories;
    }
}
