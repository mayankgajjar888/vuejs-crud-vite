# vuejs-crud-vite

## Getting started

1. get a clone of the project using the below command.

    git clone https://gitlab.com/mayankgajjar888/vuejs-crud-vite.git

2. Import DB which is located in the root directory.

3. Now install Laravel dependencies using the below command.

    composer install

4. Now Project setup is done, run the project below command in two separate CDMs.

    npm run dev

    php artisan serve


5. Now open the web browser and enter the following link to test the application.

    http://127.0.0.1:8000

6. Now sign up with the user and use the application.


```